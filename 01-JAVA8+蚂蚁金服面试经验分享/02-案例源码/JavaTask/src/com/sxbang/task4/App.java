package com.sxbang.task4;

import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class App {

    //传统的迭代实现
    public static long sumByForeach(long n){
        long result = 0;
        for (long i = 1L; i <= n; i++) {
            result += i;
        }
        return result;
    }

    //使用流实现
    public static long sumBySequence(long n){
        return Stream.iterate(1L, i -> i +1)
                .limit(n)
                .reduce(0L, Long::sum);
    }

    //将流转为并行流
    public static long sumByParallel(long n){
        return Stream.iterate(1L, i -> i +1)
                .limit(n)
                .parallel()
                .reduce(0L, Long::sum);
    }

    //测试性能方法
    public static long measurePerf(Function<Long, Long> adder, long n){
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            long sum = adder.apply(n);
            long duration = (System.nanoTime() - start) / 1000000;
            System.out.println("Result: " + sum);
            if (duration < fastest)
                fastest = duration;
        }
        return fastest;
    }

    //使用基本类型流，减少装箱拆箱
    public static long sumByRanged(long n){
        return LongStream.rangeClosed(1, n)
                .reduce(0L, Long::sum);
    }

    //使用并行流
    public static long sumByParallelRanged(long n){
        return LongStream.rangeClosed(1, n)
                .parallel()
                .reduce(0L, Long::sum);
    }



    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
       // System.out.println(App.sumByParallel(1000));

//        System.out.println("App::sumByForeach 消耗时间 ：" + measurePerf(App::sumByForeach, 10000000)+" ms");
//        System.out.println("App::sumBySequence 消耗时间 ：" + measurePerf(App::sumBySequence, 10000000)+" ms");
//        System.out.println("App::sumByParallel 消耗时间 ：" + measurePerf(App::sumByParallel, 10000000)+" ms");
//        System.out.println("App::sumByRanged 消耗时间 ：" + measurePerf(App::sumByRanged, 10000000)+" ms");
//        System.out.println("App::sumByParallelRanged 消耗时间 ：" + measurePerf(App::sumByParallelRanged, 10000000)+" ms");
        System.out.println("App::sumForBug 消耗时间 ：" + measurePerf(App::sumForBug, 10000000)+" ms");
    }

    //第二个坑：改变一个共享累加器
    public static long sumForBug(long n){
        BigBug bigBug = new BigBug();
        //串行结果正确
        //LongStream.rangeClosed(1, n).forEach(bigBug::add);
        //并行就是个坑
        LongStream.rangeClosed(1, n).parallel().forEach(bigBug::add);
        return bigBug.total;
    }

}
