package com.sxbang.task3;

public class Lover {

    private int age;
    private String sex;
    private int weight;
    private String name;

    public Lover(int age, String sex, int weight, String name){
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return "Lover{" +
                "sex='" + sex + '\'' +
                ", age=" + age + '\'' +
                ", weight=" + weight + '\'' +
                ", name=" + name+ '\'' +
                '}';
    }
}
