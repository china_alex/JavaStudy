package com.sxbang.task3;

import com.sxbang.task3.Lover;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/*
需求：1.列出体重大于120的征婚者姓名，按照体重升序排序
2.计算我们有多个lover？最大、最小、求和
 */
public class App {
    public static List<Lover> datas = Arrays.asList(
            new Lover(80,"man", 110, "小强"),
            new Lover(12, "man", 140, "帅刘"),
            new Lover(44, "man", 120, "小锋"),
            new Lover(34, "man", 130, "老温"),
            new Lover(24, "man", 150, "老王"),
            new Lover(18, "woman", 100, "阿C")
    );

    public static void main(String[] args) {
        //java8以前写法
        List<Lover> tempLovers = new ArrayList<>();
        for (Lover lover: datas) {
            if(lover.getWeight() > 120){
                tempLovers.add(lover);
            }
        }
        //Java8之前常用的
//        Collections.sort(tempLovers, new Comparator<Lover>() {
//            @Override
//            public int compare(Lover o1, Lover o2) {
//                return Integer.compare(o1.getWeight(),o2.getWeight());
//            }
//        });

        tempLovers.sort(new Comparator<Lover>() {
            @Override
            public int compare(Lover o1, Lover o2) {
                return Integer.compare(o1.getWeight(),o2.getWeight());
            }
        });

        List<String> loversName = new ArrayList<>();
        for (Lover lover: tempLovers) {
            loversName.add(lover.getName());
        }

        System.out.println(loversName);

        //Stream写法
        //相当于叫外卖
        List<String> newLoversName = datas.stream()
                .filter(lover -> lover.getWeight() > 120)
                .sorted(Comparator.comparing(Lover::getWeight))
                .map(Lover::getName)
                .collect(toList());

        System.out.println(newLoversName);

        //利用多核并行执行
        List<String> loversNames = datas.parallelStream()
                .filter(lover -> lover.getWeight() > 120)//谓词筛选
                .sorted(Comparator.comparing(Lover::getWeight))
                .limit(2)//截短流
                .skip(1)//跳过元素
                .map(Lover::getName)//映射
                .collect(toList());

        System.out.println(loversNames);

        //Optional代表一个容器类，代表一个值存不存在（解决null问题）
        Optional<Lover> lover = datas.stream()
                .filter(lover1 -> lover1.getSex().equals("woman"))
                .findAny();

        System.out.println(lover.get().getName());

        //归约
        int total = datas.stream().map(Lover::getAge).reduce(0, (a , b) -> a + b);
        System.out.println(total);

        //一共有多少Lover?
        //google的map-reduce模式，它很容易并行化
        int totalLover = datas.stream().map(Lover -> 1).reduce(0, (a , b) -> a + b);
        System.out.println(totalLover);

        //创建流
        Stream.of("It's ", "a ", "wonderful ", "day ").forEach(System.out::print);
        IntStream.range(1,20).forEach(i-> System.out.print(i+","));//返回一个1-19的数字流

        //流的处理顺序
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                });


        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                }).forEach(s-> System.out.println(s));

    }
}
