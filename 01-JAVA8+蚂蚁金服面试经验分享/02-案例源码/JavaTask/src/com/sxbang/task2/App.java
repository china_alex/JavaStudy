package com.sxbang.task2;

import java.util.Arrays;
import java.util.List;

/*
开始我们第一个小功能的开发，要求如下：（注意后面用户的需求一直在变）
用户第一个需求：查出全部的男性征婚者；
用户第二个需求：查出全部的女性征婚者；
用户第三个需求：查出全部的中年男性油腻征婚者（识别潜在条件age>15为中年油腻者）；
 */
public class App {
    public static List<Lover> datas = Arrays.asList(new Lover(80,"man"), new Lover(12, "man"), new Lover(18, "woman"));

    public static void main(String[] args) {
        //用户第一个需求：查出全部的男性征婚者；
        List<Lover> men = LoverUtil.findAllMan(datas);
        System.out.println(men);
        //用户第二个需求：查出全部的女性征婚者；
        List<Lover> women = LoverUtil.findAllByCretira(datas, "woman");
        System.out.println(women);
        //查出全部的中年男性油腻征婚者（识别潜在条件age>15为中年油腻者）
        List<Lover> oldMan = LoverUtil.findAllByCretira(datas, "man",15);
        System.out.println(oldMan);

        //重写需求
        List<Lover> yourMen = LoverUtil.findLovers(datas, LoverUtil::isMan);
        System.out.println(yourMen);

        //Lamdba (parameters) -> expression或(parameters) ->{ statements; }
        List<Lover> MyMen = LoverUtil.findLovers(datas, (Lover lover) -> "man".equals(lover.getSex()));
        System.out.println(MyMen);

        //查出全部的中年男性油腻征婚者（识别潜在条件age>15为中年油腻者）
        List<Lover> YourOldMan = LoverUtil.findLovers(datas, (Lover lover) -> "man".equals(lover.getSex()) && 15 < lover.getAge());
        System.out.println(YourOldMan);

        //更抽象化更普适化的实现
        List<Lover> OMen = CommonUtil.filter(datas, (Lover lover) -> "man".equals(lover.getSex()));
        System.out.println(OMen);

    }
}
