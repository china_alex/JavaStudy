package com.sxbang.task2;

public class Lover {

    private int age;
    private String sex;

    public Lover(int age, String sex){
        this.age = age;
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    public String toString() {
        return "Lover{" +
                "sex='" + sex + '\'' +
                ", age=" + age +
                '}';
    }
}
