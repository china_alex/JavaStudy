package com.sxbang.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class LoverUtil {

    public static List<Lover> findAllMan(List<Lover> lovers){
        List<Lover> result = new ArrayList<>();
        for (Lover lover: lovers){
            if ("man".equals(lover.getSex())) {
                result.add(lover);
            }
        }
        return result;
    }

    public static List<Lover> findAllByCretira(List<Lover> lovers, String sex){
        List<Lover> result = new ArrayList<>();
        for (Lover lover: lovers){
            if (sex.equals(lover.getSex())) {
                result.add(lover);
            }
        }
        return result;
    }

    public static List<Lover> findAllByCretira(List<Lover> lovers, String sex, int age){
        List<Lover> result = new ArrayList<>();
        for (Lover lover: lovers){
            if (sex.equals(lover.getSex()) && age < lover.getAge()) {
                result.add(lover);
            }
        }
        return result;
    }

    //谓词
    public static List<Lover> findLovers(List<Lover> lovers, Predicate<Lover> p){
        List<Lover> result = new ArrayList<Lover>();
        for(Lover lover : lovers){
            if(p.test(lover))
                result.add(lover);
        }
        return result;
    }

    public static boolean isMan(Lover lover) {
        return "man".equals(lover.getSex());
    }



}
