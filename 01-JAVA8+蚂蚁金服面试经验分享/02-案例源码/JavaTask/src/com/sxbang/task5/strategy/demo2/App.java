package com.sxbang.task5.strategy.demo2;

import com.sxbang.task5.strategy.IsAllUpperCase;
import com.sxbang.task5.strategy.IsNumeric;
import com.sxbang.task5.strategy.Validator;

public class App {
    public static void main(String[] args) {

        Fee v1 = new Fee(new DiscountFeeStrategy());
        System.out.println(v1.execute(200,3));

        Fee v2 = new Fee(new OriginalFeeStrategy());
        System.out.println( v2.execute(200, 3));

        //使用Lambda,跟简洁易读,避免僵化的模板代码
        Fee v3 = new Fee((double price, double num) -> price * num);
        System.out.println(v3.execute(200, 3));

        Fee v4 = new Fee((double price, double num) -> price * num * 0.7);
        System.out.println(v4.execute(200, 3));
    }
}
