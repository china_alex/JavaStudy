package com.sxbang.task5.strategy;

public class App {

    public static void main(String[] args) {
        Validator v1 = new Validator(new IsAllUpperCase());
        boolean isUpper = v1.validate("a");

        Validator v2 = new Validator(new IsNumeric());
        boolean isNumeric = v2.validate("6b");

        //使用Lambda,跟简洁易读,避免僵化的模板代码
        Validator v3 = new Validator((String s) -> s.matches("[A-z]+"));
        boolean isUppers = v3.validate("a");

        Validator v4 = new Validator((String s) -> s.matches("[//d]+"));
        boolean isNumerics = v4.validate("6b");
    }
}
