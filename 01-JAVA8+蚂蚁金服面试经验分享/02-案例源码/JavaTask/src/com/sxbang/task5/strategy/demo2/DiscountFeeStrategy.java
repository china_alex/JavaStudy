package com.sxbang.task5.strategy.demo2;

public class DiscountFeeStrategy implements FeeStrategy {
    @Override
    public double execute(double price, double num) {
        return price * num * 0.7;
    }
}
