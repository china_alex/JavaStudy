package com.sxbang.task5.strategy.demo2;

public class Fee {
    private final FeeStrategy feeStrategy;

    public Fee(FeeStrategy v){
        this.feeStrategy = v;
    }

    public double execute(double price, double num){
        return feeStrategy.execute(price, num);
    }
}
