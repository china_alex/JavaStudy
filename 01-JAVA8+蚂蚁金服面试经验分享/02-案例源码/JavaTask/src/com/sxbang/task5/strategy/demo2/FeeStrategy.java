package com.sxbang.task5.strategy.demo2;

public interface FeeStrategy {
    double execute(double price, double num);
}
