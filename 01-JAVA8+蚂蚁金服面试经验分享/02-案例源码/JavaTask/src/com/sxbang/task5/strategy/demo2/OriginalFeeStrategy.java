package com.sxbang.task5.strategy.demo2;

public class OriginalFeeStrategy implements FeeStrategy {
    @Override
    public double execute(double price, double num) {
        return price * num;
    }
}
