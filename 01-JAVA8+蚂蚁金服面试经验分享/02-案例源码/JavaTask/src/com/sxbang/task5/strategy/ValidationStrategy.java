package com.sxbang.task5.strategy;

public interface ValidationStrategy {
    boolean execute(String s);
}
