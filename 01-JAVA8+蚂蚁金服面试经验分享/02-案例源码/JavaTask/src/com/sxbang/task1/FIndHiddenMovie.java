package com.sxbang.task1;

import java.io.File;
import java.io.FileFilter;

public class FIndHiddenMovie {

    //找出硬盘中所有隐藏文件
    public static void main(String[] args) {
        //传统写法
        File[] files = new File(".").listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isHidden();
            }
        });

        //方法引用
        File[] newfiles = new File(".").listFiles(File::isHidden);

        for(File file : files) {
            System.out.println(file.getName());
        }

        for(File file : newfiles) {
            System.out.println(file.getName());
        }
    }

}
