package com.sxbang.task6;

import com.sxbang.task6.Lover;

import java.util.Arrays;
import java.util.List;

public class DebugLambda {
    public static void main(String[] args) {
        List<Lover> datas = Arrays.asList(new Lover(12, "man", 140, "帅刘"),null);
        datas.stream()
                .map(lover -> lover.getName())
                //.map(Lover::getName)
                .forEach(System.out::println);


        List<Integer> numbers = Arrays.asList(2,3,4,5,6);
        numbers.stream()
                .map(x -> x + 1)
                .peek(x -> System.out.println(x))
                .filter(x -> x % 2 == 0)
                .limit(2)
                .forEach(System.out::println);
    }

}
