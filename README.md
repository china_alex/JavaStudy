### 项目简介：
本项目学习围绕JAVA方向前后端分离技术，依托轻量级权限管理系统案例，基于Spring Boot、Vue.js、Vue-Element-Admin等主流技术实现的，达到快速掌握前后端分离开发的技术能力。

### 项目架构：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0623/111726_dbc6e7f4_701086.png "01-权限管理架构图.png")

### 内容更新：

01-JAVA8核心基础+蚂蚁金服面试题

02-Spring Security + JWT

03-Spring Data JPA

04-VueJS + ElementUI

### 配套视频：
配套视频地址：http://sxbang.ke.qq.com/